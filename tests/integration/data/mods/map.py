def __init__(hub):
    hub.mods.map.MAP = hub.pop.data.map()


def collect_setitem(hub):
    hub.mods.map.load_setitem()


def load_setitem(hub):
    hub.mods.map.MAP["item"] = "value"


def collect_setattr(hub):
    hub.mods.map.load_setattr()


def load_setattr(hub):
    hub.mods.map.MAP.item = "value"


def collect_setitem_nested(hub):
    hub.mods.map.load_setitem_nested()


def load_setitem_nested(hub):
    hub.mods.map.MAP["item"] = {"a": {"b": {}}}


def collect_setattr_nested(hub):
    hub.mods.map.load_setattr_nested()


def load_setattr_nested(hub):
    hub.mods.map.MAP.item.a.b = {}
