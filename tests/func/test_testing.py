# -*- coding: utf-8 -*-

from unittest.mock import sentinel, NonCallableMock
import pytest
import inspect

import pop.mods.pop.testing as testing
from pop.hub import Hub


@pytest.fixture(scope="class")
def hub(hub):
    hub.pop.sub.add("tests.mods")
    yield hub


class TestLazyPop:
    def test_hub_storage(self, hub, lazy_hub):
        assert lazy_hub._hub() is hub
        assert lazy_hub._lazy_hub() is lazy_hub
        assert lazy_hub._LazyPop__lut[hub] is lazy_hub

        assert lazy_hub.pop._hub() is hub

    def test_lazy(self, mock_hub):
        assert len(mock_hub._LazyPop__lut) == 3
        mock_hub.mods
        assert len(mock_hub._LazyPop__lut) == 4
        mock_hub.mods.testing
        assert len(mock_hub._LazyPop__lut) == 5
        mock_hub.mods.testing.echo
        assert len(mock_hub._LazyPop__lut) == 6

    def test_reset(self, mock_hub):
        mtesting = mock_hub.mods.testing
        echo = mock_hub.mods.testing.echo
        noparam = mock_hub.mods.testing.noparam
        assert len(mock_hub._LazyPop__lut) == 7

        mock_hub.mods.testing._reset()

        assert len(mock_hub._LazyPop__lut) == 5
        assert mock_hub.mods.testing is mtesting
        assert mock_hub.mods.testing.echo is not echo
        assert mock_hub.mods.testing.noparam is not noparam

    def test_recursive_reset(self, mock_hub):
        mtesting = mock_hub.mods.testing
        mtest = mock_hub.mods.test
        assert len(mock_hub._LazyPop__lut) == 6
        mock_hub.mods.testing.echo
        mock_hub.mods.test.ping
        assert len(mock_hub._LazyPop__lut) == 8

        mock_hub.mods._reset()
        assert len(mock_hub._LazyPop__lut) == 4
        assert mock_hub.mods.testing is not mtesting
        assert mock_hub.mods.test is not mtest

    def test_reset_assigment(self, mock_hub):
        aosnthu = object()
        mock_hub.mods.aosnthu = aosnthu
        mock_hub.mods._reset()

        assert not hasattr(mock_hub.mods, "aosnthu")

    @pytest.mark.skip
    def test_reset_coupling(self, hub):
        obj = object()
        hub.obj = obj
        hub.pop.obj = obj

        mock_hub = hub.pop.testing.mock_hub()

        l_obj1 = mock_hub.obj
        assert mock_hub.pop.obj is mock_hub.obj

        mock_hub.pop._reset()
        l_obj2 = mock_hub.obj
        assert mock_hub.pop.obj is mock_hub.obj
        assert l_obj1 is not l_obj2

    def test_in_dict(self, lazy_hub):
        # for autocompletion, all attributes should exist, even if it hasn't been called.
        assert "mods" in lazy_hub.__dict__

    def test_duplicate_object(self, hub):
        hub.test_val = sentinel.test_val
        hub.mods.test_val = sentinel.test_val
        hub.mods.testing.test_val = sentinel.test_val

        l_hub = testing._LazyPop(hub)

        assert isinstance(l_hub.test_val, NonCallableMock)
        assert l_hub.test_val is l_hub.mods.test_val
        assert l_hub.mods.test_val is l_hub.mods.testing.test_val

    def test_duplicate_hub(self, hub):
        hub.hub = hub
        hub.mods.hub = hub
        hub.mods.foo.hub = hub

        l_hub = testing._LazyPop(hub)

        assert l_hub.hub is l_hub
        assert l_hub.mods.hub is l_hub
        assert l_hub.mods.foo.hub is l_hub

    def test_recursive_subs(self, hub):
        hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
        l_hub = testing._LazyPop(hub)

        assert hub.mods.nest.basic.ret_true()

        with pytest.raises(NotImplementedError):
            l_hub.mods.nest.basic.ret_true()

    def test_var_exists_enforcement(self, hub):
        hub.FOO = "foo"
        hub.mods.FOO = "foo"
        hub.mods.testing.FOO = "foo"

        l_hub = testing._LazyPop(hub)

        for _ in (l_hub, l_hub.mods, l_hub.mods.testing):
            with pytest.raises(AttributeError):
                l_hub.BAZ

    def test_recursive_get(self, hub):
        assert hub.mods
        l_hub = testing._LazyPop(hub)

        result = getattr(l_hub, "mods.foo")
        assert result is l_hub.mods.foo

    def test_find_subs(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")
        l_hub = testing._LazyPop(hub)

        subs = l_hub._find_subs()
        assert len(subs) == 2
        assert getattr(hub, subs[0][0]) is subs[0][1]

        # test nested subs
        hub.pop.sub.add(dyne_name="dn1")
        hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
        l_hub = testing._LazyPop(hub)
        subs = l_hub._find_subs()
        assert len(subs) == 6
        assert len(subs[5][0].split(".")) == 4
        assert getattr(hub, subs[5][0]) is subs[5][1]

    def test_resolve_this(self, mock_hub):
        from tests.mods.test import this, double_underscore

        assert mock_hub.mods.test.ping() is this(mock_hub)
        double_underscore(mock_hub)


class TestMockHub:
    def test_mock_hub_dereference_errors(self, mock_hub):
        with pytest.raises(AttributeError, match="has no attribute 'nosub'"):
            mock_hub.nosub.nomodule.nofunc()

        with pytest.raises(AttributeError, match="has no attribute 'nomodule'"):
            mock_hub.mods.nomodule.nofunc()

        with pytest.raises(AttributeError, match="has no attribute 'nofunc'"):
            mock_hub.mods.testing.nofunc()

    def test_mock_hub_function_enforcement(self, mock_hub):
        with pytest.raises(TypeError, match="missing a required argument: 'param'"):
            mock_hub.mods.testing.echo()

    def test_mock_hub_return_value(self, mock_hub):
        mock_hub.mods.testing.echo.return_value = sentinel.myreturn
        assert mock_hub.mods.testing.echo("param") is sentinel.myreturn

    @pytest.mark.asyncio
    async def test_async_echo(self, hub, mock_hub):
        val = "foo"
        assert await hub.mods.testing.async_echo(val) == val

        mock_hub.mods.testing.async_echo.return_value = val
        assert await mock_hub.mods.testing.async_echo(val + "change") == val

    def test_signature(self, hub, mock_hub):
        assert inspect.signature(hub.pop.sub.add) == inspect.signature(
            mock_hub.pop.sub.add
        )


class TestNoContractHub:
    def test_call(self, fn_hub):
        val = fn_hub.mods.testing.echo(sentinel.param)
        assert val is sentinel.param

    def test_signature(self, hub, fn_hub):
        assert inspect.signature(hub.pop.sub.add) == inspect.signature(
            fn_hub.pop.sub.add
        )


class TestMockContracted:
    @staticmethod
    @pytest.fixture(scope="class")
    def hub(hub):
        hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
        yield hub

    def test_hub_contract(self, hub):
        assert hub.mods.testing.echo("foo") == "contract foo"

    def test_contract_hub_contract(self, hub):
        m_echo = testing.mock_contracted(None, hub.mods.testing.echo)
        m_echo.func.return_value = "bar"
        assert m_echo("foo") == "contract bar"

    def test_contract_hub_getattr(self, hub):
        assert testing.mock_contracted(None, hub.mods.testing.echo).return_value

    def test_contract_hub_module(self, hub):
        m_echo = testing.mock_contracted(None, hub.mods.testing.echo)
        func_module = hub.mods.testing.echo.func.__module__
        assert m_echo.func.__module__ == func_module

    def test_signature(self, hub):
        m_sig = testing.mock_contracted(None, hub.mods.testing.signature_func)
        assert str(m_sig.signature) == "(hub, param1, param2='default')"

    def test_get_arguments(self, hub):
        m_sig = testing.mock_contracted(None, hub.mods.testing.signature_func)
        m_sig("passed in")

    def test_copy_func_attributes(self, hub):
        echo = testing.mock_contracted(None, hub.mods.testing.echo)
        attr_func = testing.mock_contracted(None, hub.mods.testing.attr_func)

        with pytest.raises(AttributeError):
            assert echo.func.test
        assert attr_func.func.test is True

        with pytest.raises(AttributeError):
            assert echo.func.__test__
        assert attr_func.func.__test__ is True


class TestContractHub:
    @staticmethod
    @pytest.fixture(scope="class")
    def hub(hub):
        hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
        yield hub

    def test_hub_contract(self, hub):
        assert hub.mods.testing.echo("foo") == "contract foo"

    def test_contract_hub_contract(self, contract_hub):
        assert isinstance(contract_hub.mods.testing.echo, testing.Contracted)

    @pytest.mark.asyncio
    async def test_async_echo(self, hub, contract_hub):
        val = "foo"
        expected = "async contract " + val
        assert await hub.mods.testing.async_echo(val) == expected

        contract_hub.mods.testing.async_echo.func.return_value = val
        assert await contract_hub.mods.testing.async_echo(val + "change") == expected

    def test_contract_hub_inspect(self, contract_hub):
        # demo ways that we can inspect the contract system
        assert len(contract_hub.mods.testing.echo.contracts) == 1
        assert "call_signature_func" in dir(contract_hub.mods.testing.echo.contracts[0])

    def test_contract_hub_modify(self, contract_hub, hub):
        # modifying the contract
        contract_hub.mods.testing.echo.func.return_value = "bar"
        assert contract_hub.mods.testing.echo("foo") == "contract bar"
        contract_hub.mods.testing.echo.contract_functions["call"] = []
        assert contract_hub.mods.testing.echo("foo") == "bar"

        # verify that modification didn't mess with the real hub:
        assert hub.mods.testing.echo("foo") == "contract foo"


class TestMockAttrHub:
    @staticmethod
    @pytest.fixture(scope="class")
    def hub(hub):
        hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
        yield hub

    def test_hub_contract(self, mock_attr_hub, hub):
        assert hub.mods.testing.echo("foo") == "contract foo"
        assert mock_attr_hub.mods.testing.echo("foo") == "contract foo"

        hub.VAL = {}
        assert "Mock" in str(mock_attr_hub.OPT)


class TestHybridHub:
    """
    We override assignment so that if you take two types of test hubs, you can assign freely between them:

      mock_hub.sub.mod = fn_hub.sub.mod

    This will create a hybrid test hub.

    Everything below mock_hub.sub.mod will be generated as fn_hub, everything else using mock_hub.
    """

    def test_contracted_assignment(self, mock_hub, fn_hub):
        mock_hub.pop.sub.add = fn_hub.pop.sub.add

        assert mock_hub.pop.sub.add.hub is mock_hub

    def test_mod_assignment(self, mock_hub, fn_hub):
        mock_hub.pop.testing = fn_hub.pop.testing

        assert mock_hub.pop.testing.__class__ is fn_hub.__class__
        assert mock_hub.pop.testing.fn_hub.hub is mock_hub

    def test_sub_assignment(self, mock_hub, fn_hub):
        mock_hub.pop = fn_hub.pop

        assert mock_hub.pop._lazy_hub() is mock_hub
        assert mock_hub.pop.__class__ is fn_hub.pop.__class__
        assert mock_hub.pop.testing._lazy_hub() is mock_hub
        assert mock_hub.pop.testing.__class__ is mock_hub.pop.__class__

    def test_real_hub_assignment(self, mock_hub, hub):
        # contracted works
        mock_hub.pop.sub.add = hub.pop.sub.add
        assert mock_hub.pop.sub.add.hub is mock_hub

        match = (
            "Mixing of real and test hubs is not supported. "
            + "Contracteds \\(hub.sub.mod.func\\) are supported."
        )
        with pytest.raises(TypeError, match=match):
            mock_hub.pop.sub = hub.pop.sub

        with pytest.raises(TypeError, match=match):
            mock_hub.pop = hub.pop
