import pathlib
import pytest
import sys


def pytest_sessionstart(session: pytest.Session):
    root = pathlib.Path(session.config.rootdir)

    CODE_DIR = str(root)
    if CODE_DIR in sys.path:
        sys.path.remove(CODE_DIR)
    sys.path.insert(0, CODE_DIR)

    TPATH_DIR = str(root.joinpath("tests").joinpath("tpath"))
    if TPATH_DIR in sys.path:
        sys.path.remove(TPATH_DIR)
    sys.path.insert(0, TPATH_DIR)
